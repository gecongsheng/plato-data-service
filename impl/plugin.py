class Plugin:
    """
    插件接口
    """
    def __init__(self, frame):
        """
        构造

        Args
            frame 框架引用
        """
        self.frame = frame

    def start(self):
        """
        插件初始化
        """
        return True

    def stop(self):
        """
        插件关闭
        """
        pass

    def tick(self):
        """
        每帧调用一次
        """
        pass

    def save(self, json_obj):
        return True

    def load(self, json_obj):
        return None

    def remove(self, json_obj):
        return 0


class CachePlugin(Plugin):
    """
    缓存插件接口
    """
    def __init__(self, frame):
        super().__init__(frame)

    def sync(self, type_name, json_obj):
        return True
