import queue
import threading
import time
import multiprocessing


class AsyncTask:
    """
    异步任务
    """

    def DoTask(self):
        pass


class QueueThread(threading.Thread):
    """
    线程池, 用于执行异步任务
    """

    def __init__(self, frame):
        threading.Thread.__init__(self)
        self.frame = frame
        self._input_queue = queue.Queue()
        self._output_queue = queue.Queue()
        self._stop_flag = False

    def push(self, task):
        self._input_queue.put(task)

    def get_no_wait(self):
        try:
            return self._output_queue.get_nowait()
        except queue.Empty:
            return None

    def stop(self):
        self._stop_flag = True

    def run(self):
        while not self._stop_flag:
            try:
                task: AsyncTask = self._input_queue.get_nowait()
                task.DoTask()
                self._output_queue.put(task)
            except queue.Empty:
                time.sleep(0.01)
                pass
            except Exception as e:
                self.frame.write_error_log(str(e))


def create_worker(frame):
    return QueueThread(frame)


class AsyncTaskThreadingPool:
    def __init__(self, frame, pool_size=multiprocessing.cpu_count(), WorkerFactory=create_worker):
        self.frame = frame
        self.worker_factory = WorkerFactory
        if pool_size <= 0:
            pool_size = multiprocessing.cpu_count()
        if pool_size < 2:
            pool_size = 1
        self._pool_size = pool_size
        self._pool = []
        self._pool_index = 0
        for i in range(0, self._pool_size):
            worker = self.worker_factory(frame)
            self._pool.append(worker)
            worker.start()

    def push(self, task):
        if self._pool_index >= self._pool_size:
            self._pool_index = 0
        self._pool[self._pool_index].push(task)
        self._pool_index += 1

    def get_no_wait(self):
        for i in range(0, self._pool_size):
            result = self._pool[i].get_no_wait()
            if result is not None:
                return result
        return None

    def stop(self):
        for thr in self._pool:
            thr.stop()
            thr.join()
        self._pool.clear()

    def __del__(self):
        self.stop()
